#!/bin/bash
set -e

CHECK_URL=${CHECK_URL:-https://mydatakeeper.co/}

arp_check() {
    local gateway=
    if ! gateway=$(/usr/bin/ip route | /usr/bin/sed -n 's/^default via \([0-9.]*\) .*$/\1/p' | /usr/bin/head -n1); then
        >&2 echo 'Default gateway is not set'
        return 1
    fi

    local retry=5
    while [ "$retry" -gt 0 ]; do
        if /usr/bin/arping -f -c 2 -w 5 "$gateway" >/dev/null; then
            echo "Gateway '${gateway}' reached via ARP"
            return 0
        fi

        retry=$((retry-1))
        >&2 echo "Gateway '${gateway}' cannot be reached via ARP (trial $((5-retry)))"
        /usr/bin/sleep 5
    done

    return 1
}

http_check() {
    local retry=5
    while [ "$retry" -gt 0 ]; do
        if /usr/bin/curl -s -f -I "$CHECK_URL"; then
            >&2 echo "Check URL '${CHECK_URL}' reached via HTTP over TCP/IP"
            return 0
        fi

        retry=$((retry-1))
        >&2 echo "Check URL '${CHECK_URL}' cannot be reached via HTTP over TCP/IP (trial $((5-retry)))"
        /usr/bin/sleep 5
    done

    return 1
}

while /usr/bin/true; do
    for i in $(/usr/bin/seq 120); do
        arp_check || exit 1
        /usr/bin/sleep 5
    done
    http_check || exit 1
done
